package ca.miracles.register.utils;

/**
 * Customized Spinner to user an Integer value but display a String value on the UI.
 * @author  Dan Richard
 * Created by Dan on 15/02/2015.
 */
public class SpinnerObject {
    private  int databaseId;
    private String databaseValue;

    public SpinnerObject ( int databaseId , String databaseValue ) {
        this.databaseId = databaseId;
        this.databaseValue = databaseValue;
    }

    public int getId () {
        return databaseId;
    }

    public String getValue () {
        return databaseValue;
    }

    @Override
    public String toString () {
        return databaseValue;
    }
}
