package ca.miracles.register.utils;

import android.database.Cursor;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author  Dan Richard
 * @since   2015-02-11
 */
public class Converters {

    /**
     * Converts a DatePickerDialog values (ints) to a formatted date for display.
     * @param year  Int representing the Year.
     * @param month Int representing the Month.
     * @param day   Int representing the Day.
     * @return      String formatted to "MMM dd yyyy".
     */
    public static String dateDisplay(int year, int month, int day) {
        String sYear, sMonth, sDay;

        sYear = String.valueOf(year);

        DateFormat formatter = new SimpleDateFormat("MMM");
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MONTH, month-1);
        sMonth = formatter.format(calendar.getTime());

        if(day < 10) {
            sDay = "0" + String.valueOf(day);
        } else {
            sDay = String.valueOf(day);
        }

        return sMonth + " " + sDay + " " + sYear;
    }


    /**
     * Converts a formatted date String to a Calendar object.
     * @param date  Formatted date String (MMM dd yyyy).
     * @return      Calendar object representing the date.
     */
    public static Calendar stringToCalendar(String date) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy");
        try {
            cal.setTime(sdf.parse(date));
            return cal;
        } catch(ParseException e) {
            Log.e("Error parsing date", e.getMessage());
            return null;
        }
        // Convert a String date value to a Calendar object.
    }


    /**
     * Convert a Calendar date value to a Date, then to a Long for insertion into SQLite.
     * @param cal   Calendar object that needs to be converted to a Long.
     * @return      Date value converted to a Long.
     */
    public static Long calendarToLong(Calendar cal) {
        if (cal != null) {
            // Call getTime() twice, first to convert Calendar to Date, then Date to Long.
            return cal.getTime().getTime();
        }
        return null;
    }


    /**
     * Convert a Long value to a Calendar object.
     * @param cursor    Cursor (dataset) from SQLite.
     * @param index     Index of the column containing the Long date value to convert.
     * @return          Date converted to a Calendar.
     */
    public static Calendar longToCalendar(Cursor cursor, int index) {
        if (cursor.isNull(index)) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cursor.getLong(index));

        return cal;
    }


    /**
     * Converts an integer value to a boolean String value.
     * @param chkVal    Boolean value.
     * @return          String representing a boolean value (Y/N).
     */
    public static String booleanToString(int chkVal) {
        if(chkVal > 0) {
            return "Y";
        } else {
            return "N";
        }
    }
}
