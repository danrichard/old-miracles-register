package ca.miracles.register.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author  Dan Richard
 * @since   2015-02-09
 */
public class SQLiteHelper extends SQLiteOpenHelper {
    // Child Table Columns
    public static final String TABLE_CHILDREN = "children";
    public static final String CHILD_COLUMN_ID = "_id";
    public static final String COLUMN_FIRSTNAME = "first_name";
    public static final String COLUMN_LASTNAME = "last_name";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_ROOM = "room";
    public static final String COLUMN_BIRTHDATE = "birth_date";
    public static final String COLUMN_WAITINGLIST = "waiting_list";
    public static final String COLUMN_WAITINGLISTDATE = "waiting_list_date";

    // Room Table Columns
    public static final String TABLE_ROOMS = "rooms";
    public static final String ROOM_COLUMN_ID = "_id";
    public static final String COLUMN_ROOM_NAME = "room_name";
    public static final String COLUMN_CURRENT_STAFF = "num_of_staff";
    public static final String COLUMN_FROM_AGE = "from_age";
    public static final String COLUMN_TO_AGE = "to_age";
    public static final String COLUMN_LOGO_PATH = "logo_path";

    // Room Enrolment Columns
    public static final String TABLE_ENROLMENT = "enrolment";
    public static final String ENROLMENT_COLUMN_ID = "_id";
    public static final String COLUMN_ROOM_ID = "room_id";
    public static final String COLUMN_CHILD_ID = "child_id";


    // DB Metadata
    public static final String DATABASE_NAME = "children.db";
    public static final int DATABASE_VERSION = 1;

    // Table CREATE Statements
    private static final String CREATE_CHILDREN = "create table "
            + TABLE_CHILDREN + "("
            + CHILD_COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_FIRSTNAME + " text not null, "
            + COLUMN_LASTNAME + " text not null, "
            + COLUMN_PHONE + " text, "
            + COLUMN_ROOM + " text, "
            + COLUMN_BIRTHDATE + " text not null, "
            + COLUMN_WAITINGLIST + " integer, "
            + COLUMN_WAITINGLISTDATE + " text);";

    private static final String CREATE_ROOMS = "create table "
            + TABLE_ROOMS + "("
            + ROOM_COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_ROOM_NAME + " text not null, "
            + COLUMN_CURRENT_STAFF + " integer not null, "
            + COLUMN_FROM_AGE + " integer not null, "
            + COLUMN_TO_AGE + " integer not null, "
            + COLUMN_LOGO_PATH + " text);";

    private static final String CREATE_ENROLMENT = "create table "
            + TABLE_ENROLMENT + "("
            + ENROLMENT_COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_ROOM_ID + " integer not null, "
            + COLUMN_CHILD_ID + " integer not null);";

    /**
     * Class constructor. Call parent class to manage database creation and version management
     * @param context
     */
    public SQLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * Execute statements to create the database tables
     * @param database
     */
    @Override
    public void onCreate(SQLiteDatabase database){

        database.execSQL(CREATE_CHILDREN);
        database.execSQL(CREATE_ROOMS);
        database.execSQL(CREATE_ENROLMENT);
    }


    /**
     * If the database version changes (upgrade), drop the existing table and re-create it
     * NOTE: All data will be lost. Concessions need to be made to save/restore data if needed.
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        Log.w(SQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to " + newVersion
                + ", which will destroy all old data.");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHILDREN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROOMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENROLMENT);
        onCreate(db);
    }
}