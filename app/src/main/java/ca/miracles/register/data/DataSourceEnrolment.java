package ca.miracles.register.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ca.miracles.register.models.Enrolment;

/**
 * @author  Dan Richard
 * @since   2015-02-15
 */
public class DataSourceEnrolment {
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    // String array to hold the list of columns used in the QUERY statements.
    private String[] allColumns = {SQLiteHelper.ENROLMENT_COLUMN_ID,
            SQLiteHelper.COLUMN_ROOM_ID, SQLiteHelper.COLUMN_CHILD_ID};


    /**
     * Class constructor. Instantiate an SQLiteHelper object.
     * @param context
     */
    public DataSourceEnrolment(Context context) {
        dbHelper = new SQLiteHelper(context);
    }


    /**
     * If the database already exists, open it for reading/writing.
     * Otherwise, create the database and open it for reading/writing.
     * @throws java.sql.SQLException
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }


    /**
     * Close the data connection.
     */
    public void close() {
        dbHelper.close();
    }


    /**
     * Insert a new Enrolment record, then return the record as an Enrolment object.
     * @param roomId
     * @param childId
     * @return Enrolment object representing the record that was inserted in the db
     */
    public Enrolment createEnrolment(int roomId, int childId) {
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_ROOM_ID, roomId);
        values.put(SQLiteHelper.COLUMN_CHILD_ID, childId);

        // Insert the Enrolment record.
        long insertId = database.insert(SQLiteHelper.TABLE_ENROLMENT, null, values);

        // Retrieve the record that was just inserted and save it to a new Enrolment object.
        Cursor cursor = database.query(SQLiteHelper.TABLE_ENROLMENT, allColumns, SQLiteHelper.ENROLMENT_COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Enrolment newEnrolment = cursorToEnrolment(cursor);
        cursor.close();
        return newEnrolment;
    }


    /**
     * Delete the Enrolment record from the database.
     * @param enrolment
     */
    public void deleteEnrolment(Enrolment enrolment) {
        long id = enrolment.getId();
        //TODO: Write this out to a LOG entry rather than println
        System.out.println("Enrolment deleted with id: " + id);
        database.delete(SQLiteHelper.TABLE_ENROLMENT, SQLiteHelper.ENROLMENT_COLUMN_ID + " = " + id, null);
    }


    /**
     * Query all records from the table.
     * Iterate through the rows and create a list of Enrolment objects.
     * @return List of Enrolment objects
     */
    public List<Enrolment> getAllEnrolments() {
        List<Enrolment> enrolments = new ArrayList<Enrolment>();

        // Retrieve all records from the table.
        Cursor cursor = database.query(SQLiteHelper.TABLE_ENROLMENT, allColumns, null, null, null, null, null);

        // Iterate through the dataset, adding to the list of Enrolment objects.
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Enrolment enrolment = cursorToEnrolment(cursor);
            enrolments.add(enrolment);
            cursor.moveToNext();
        }
        cursor.close();
        return enrolments;
    }


    /**
     * Query all enrolments for the provided Room.
     * @param roomId    Room ID to (key) to query for enrolments on.
     * @return          List of Enrolments for the provided Room
     */
    public List<Enrolment> getEnrolmentsByRoom(int roomId) {
        List<Enrolment> enrolments = new ArrayList<Enrolment>();

        // Retrieve all records from the table.
        Cursor cursor = database.query(SQLiteHelper.TABLE_ENROLMENT, allColumns, SQLiteHelper.COLUMN_ROOM_ID + " = " + roomId, null, null, null, null);

        // Iterate through the dataset, adding to the list of Enrolment objects.
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Enrolment enrolment = cursorToEnrolment(cursor);
            enrolments.add(enrolment);
            cursor.moveToNext();
        }
        cursor.close();
        return enrolments;
    }


    /**
     * Convert a database record to a Enrolment object.
     * @param cursor
     * @return Enrolment object representing the database row
     */
    private Enrolment cursorToEnrolment(Cursor cursor) {
        Enrolment enrolment = new Enrolment();
        enrolment.setId(cursor.getLong(0));
        enrolment.setRoomId(cursor.getInt(1));
        enrolment.setChildId(cursor.getInt(2));
        return enrolment;
    }
}
