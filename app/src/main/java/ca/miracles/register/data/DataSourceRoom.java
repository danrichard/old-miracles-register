package ca.miracles.register.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ca.miracles.register.models.Room;
import ca.miracles.register.utils.SpinnerObject;

/**
 * @author  Dan Richard
 * @since   2015-02-15
 */
public class DataSourceRoom {
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    // String array to hold the list of columns used in the QUERY statements.
    private String[] allColumns = {SQLiteHelper.ROOM_COLUMN_ID, SQLiteHelper.COLUMN_ROOM_NAME, SQLiteHelper.COLUMN_CURRENT_STAFF,
            SQLiteHelper.COLUMN_FROM_AGE, SQLiteHelper.COLUMN_TO_AGE, SQLiteHelper.COLUMN_LOGO_PATH};


    /**
     * Class constructor. Instantiate an SQLiteHelper object.
     * @param context
     */
    public DataSourceRoom(Context context) {
        dbHelper = new SQLiteHelper(context);
    }


    /**
     * If the database already exists, open it for reading/writing.
     * Otherwise, create the database and open it for reading/writing.
     * @throws java.sql.SQLException
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }


    /**
     * Close the data connection.
     */
    public void close() {
        dbHelper.close();
    }


    /**
     * Insert a new Room record, then return the record as a Room object.
     * @param roomName
     * @param numOfStaff
     * @param fromAge
     * @param toAge
     * @param logoPath
     * @return Room object representing the record that was inserted in the db
     */
    public Room createRoom(String roomName, int numOfStaff, int fromAge, int toAge, String logoPath) {
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_ROOM_NAME, roomName);
        values.put(SQLiteHelper.COLUMN_CURRENT_STAFF, numOfStaff);
        values.put(SQLiteHelper.COLUMN_FROM_AGE, fromAge);
        values.put(SQLiteHelper.COLUMN_TO_AGE, toAge);
        values.put(SQLiteHelper.COLUMN_LOGO_PATH, logoPath);

        // Insert the Room record.
        long insertId = database.insert(SQLiteHelper.TABLE_ROOMS, null, values);

        // Retrieve the record that was just inserted and save it to a new Room object.
        Cursor cursor = database.query(SQLiteHelper.TABLE_ROOMS, allColumns, SQLiteHelper.ROOM_COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Room newRoom = cursorToRoom(cursor);
        cursor.close();
        return newRoom;
    }


    /**
     * Delete the Room record from the database.
     * @param room
     */
    public void deleteRoom(Room room) {
        long id = room.getId();
        //TODO: Write this out to a LOG entry rather than println
        System.out.println("Room deleted with id: " + id);
        database.delete(SQLiteHelper.TABLE_ROOMS, SQLiteHelper.ROOM_COLUMN_ID + " = " + id, null);
    }


    /**
     * Query all records from the table.
     * Iterate through the rows and create a list of Room objects.
     * @return List of Room objects
     */
    public List<Room> getAllRooms() {
        List<Room> rooms = new ArrayList<Room>();

        // Retrieve all records from the table.
        Cursor cursor = database.query(SQLiteHelper.TABLE_ROOMS, allColumns, null, null, null, null, null);

        // Iterate through the dataset, adding to the list of Room objects.
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Room room = cursorToRoom(cursor);
            rooms.add(room);
            cursor.moveToNext();
        }
        cursor.close();
        return rooms;
    }


    /**
     * Convert a database record to a Room object.
     * @param cursor
     * @return Room object representing the database row
     */
    private Room cursorToRoom(Cursor cursor) {
        Room room = new Room();
        room.setId(cursor.getLong(0));
        room.setRoomName(cursor.getString(1));
        room.setNumOfStaff(cursor.getInt(2));
        room.setFromMonths(cursor.getInt(3));
        room.setToMonths(cursor.getInt(4));
        room.setLogoPath(cursor.getString(5));
        return room;
    }


    /**
     * Create a list of objects to populate value/text pairs for spinners.
     * @return  List of SpinnerObjects.
     */
    public List<SpinnerObject> getRoomsForSpinner(){
        List <SpinnerObject> rooms = new ArrayList<SpinnerObject>();
        // Select All Query
        Cursor cursor = database.query(SQLiteHelper.TABLE_ROOMS, allColumns, null, null, null, null, null);

        // Loop through all rows and add values to the list
        if (cursor.moveToFirst()) {
            do {
                rooms.add(new SpinnerObject(cursor.getInt(0), cursor.getString(1)));
            } while(cursor.moveToNext());
        }

        cursor.close();
        return rooms;
    }
}
