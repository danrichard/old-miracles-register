package ca.miracles.register.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ca.miracles.register.models.Child;
import ca.miracles.register.utils.SpinnerObject;
import ca.miracles.register.utils.Converters;

/**
 * @author  Dan Richard
 * @since   2015-02-09
 */
public class DataSourceChild {
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    // String array to hold the list of columns used in the QUERY statements.
    private String[] allColumns = {SQLiteHelper.CHILD_COLUMN_ID, SQLiteHelper.COLUMN_FIRSTNAME,
            SQLiteHelper.COLUMN_LASTNAME, SQLiteHelper.COLUMN_PHONE, SQLiteHelper.COLUMN_BIRTHDATE,
            SQLiteHelper.COLUMN_WAITINGLIST, SQLiteHelper.COLUMN_WAITINGLISTDATE};


    /**
     * Class constructor. Instantiate an SQLiteHelper object.
     * @param context
     */
    public DataSourceChild(Context context) {
        dbHelper = new SQLiteHelper(context);
    }


    /**
     * If the database already exists, open it for reading/writing.
     * Otherwise, create the database and open it for reading/writing.
     * @throws SQLException
     */
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }


    /**
     * Close the data connection.
     */
    public void close() {
        dbHelper.close();
    }


    /**
     * Insert a new Child record, then return the record as a Child object.
     * @param firstName
     * @param lastName
     * @param phone
     * @param birthDate
     * @param waitingList
     * @param waitingListDate
     * @return Child object representing the record that was inserted in the db
     */
    public Child createChild(String firstName, String lastName, String phone, Calendar birthDate, Integer waitingList, Calendar waitingListDate) {
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.COLUMN_FIRSTNAME, firstName);
        values.put(SQLiteHelper.COLUMN_LASTNAME, lastName);
        values.put(SQLiteHelper.COLUMN_PHONE, phone);
        values.put(SQLiteHelper.COLUMN_BIRTHDATE, Converters.calendarToLong(birthDate));
        values.put(SQLiteHelper.COLUMN_WAITINGLIST, waitingList);
        values.put(SQLiteHelper.COLUMN_WAITINGLISTDATE, Converters.calendarToLong(waitingListDate));

        // Insert the Child record.
        long insertId = database.insert(SQLiteHelper.TABLE_CHILDREN, null, values);

        // Retrieve the record that was just inserted and save it to a new Child object.
        Cursor cursor = database.query(SQLiteHelper.TABLE_CHILDREN, allColumns, SQLiteHelper.CHILD_COLUMN_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Child newChild = cursorToChild(cursor);
        cursor.close();
        return newChild;
    }


    /**
     * Delete the Child record from the database.
     * @param child
     */
    public void deleteChild(Child child) {
        long id = child.getId();
        //TODO: Write this out to a LOG entry rather than println
        System.out.println("Child deleted with id: " + id);
        database.delete(SQLiteHelper.TABLE_CHILDREN, SQLiteHelper.CHILD_COLUMN_ID + " = " + id, null);
    }


    /**
     * Query all records from the table.
     * Iterate through the rows and create a list of Child objects.
     * @return List of Child objects
     */
    public List<Child> getAllChildren() {
        List<Child> children = new ArrayList<Child>();

        // Retrieve all records from the table.
        Cursor cursor = database.query(SQLiteHelper.TABLE_CHILDREN, allColumns, null, null, null, null, null);

        // Iterate through the dataset, adding to the list of Child objects.
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Child child = cursorToChild(cursor);
            children.add(child);
            cursor.moveToNext();
        }
        cursor.close();
        return children;
    }


    /**
     * Retrieve a Child record for the provided Child ID.
     * @param childId   The ID to retrieve the Child record for.
     * @return          Child record.
     */
    public Child getChild(int childId) {
        Child child = null;

        // Retrieve the Child record.
        Cursor cursor = database.query(SQLiteHelper.TABLE_CHILDREN, allColumns, SQLiteHelper.CHILD_COLUMN_ID + " = " + childId, null, null, null, null);

        // Convert the record (cursor) to a Child object.
        cursor.moveToFirst();
        if(cursor.getCount() > 0) {
            child = cursorToChild(cursor);
        }
        cursor.close();

        return child;
    }


    /**
     * Convert a database record to a Child object.
     * @param cursor
     * @return Child object representing the database row
     */
    private Child cursorToChild(Cursor cursor) {
        Child child = new Child();
        child.setId(cursor.getLong(0));
        child.setFirstName(cursor.getString(1));
        child.setLastName(cursor.getString(2));
        child.setPhone(cursor.getString(3));
        child.setBirthDate(Converters.longToCalendar(cursor, 5));
        child.setWaitingList(cursor.getInt(6));
        child.setWaitingListDate(Converters.longToCalendar(cursor, 7));
        return child;
    }


    /**
     * Create a list of objects to populate value/text pairs for spinners.
     * @return  List of SpinnerObjects.
     */
    public List<SpinnerObject> getChildrenForSpinner(){
        List <SpinnerObject> children = new ArrayList<SpinnerObject>();
        // Select All Query
        Cursor cursor = database.query(SQLiteHelper.TABLE_CHILDREN, allColumns, null, null, null, null, null);

        // Loop through all rows and add values to the list
        if (cursor.moveToFirst()) {
            do {
                children.add(new SpinnerObject(cursor.getInt(0), cursor.getString(1) + " " + cursor.getString(2)));
            } while(cursor.moveToNext());
        }

        cursor.close();
        return children;
    }
}
