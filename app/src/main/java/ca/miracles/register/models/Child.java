package ca.miracles.register.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ca.miracles.register.utils.Converters;

/**
 * @author  Dan Richard
 * @since   2015-02-09
 */
public class Child {
    private long id;
    private String firstName;
    private String lastName;
    private String phone;
    private Calendar birthDate;
    private Calendar waitingListDate;
    private Integer waitingList;
    private Float age;

    // Getters & Setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    public Calendar getWaitingListDate() {
        return waitingListDate;
    }

    public void setWaitingListDate(Calendar waitingListDate) { this.waitingListDate = waitingListDate; }

    public Integer getWaitingList() {
        return waitingList;
    }

    public void setWaitingList(Integer waitingList) {
        this.waitingList = waitingList;
    }

    public Float getAge() {
        return age;
    }

    public void setAge(Float age) {
        this.age = age;
    }


    /**
     * Text display of a Child record in the database.
     * @return  String representing a database record.
     */
    @Override
    public String toString() {
        // Convert Calendar date to a user-friendly format.
        String mDOB = null;
        String mWLDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy");
        if (birthDate != null) {
            mDOB = sdf.format(birthDate.getTime());
        }
        if (waitingListDate != null) {
            mWLDate = sdf.format(waitingListDate.getTime());
        }

        // Build a display string representing a Child object.
        String retValue = "Child: " + firstName + " " + lastName
                + ", Phone: " + phone
                + ", Birth Date: " + mDOB
                + ", Waiting List? " + Converters.booleanToString(waitingList)
                + ", Added to Waiting List on: " + mWLDate;

        return retValue;
    }


    /**
     * Returns the custom age representation of "YEARS.MONTHS" as a Float value.
     * @return  Float representation of the Child's age.
     */
    public float getAgeInYearMonthFormat() {
        Date date = new Date();

        // Set start and end dates to calculate.
        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(birthDate.getTime());
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(date);

        // Get the difference between the two dates in Months
        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        float diffMonth = endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

        if(diffMonth < 10) {
            diffMonth = diffMonth / 10;
        } else {
            diffMonth = diffMonth / 100;
        }

        return diffYear + diffMonth;
    }
}