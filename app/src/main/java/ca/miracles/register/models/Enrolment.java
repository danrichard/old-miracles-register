package ca.miracles.register.models;

/**
 * @author  Dan Richard
 * @since   2015-02-15
 */
public class Enrolment {
    private long id;
    private int roomId;
    private int childId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    /**
     * Text display of a Enrolment record in the database.
     * @return  String representing a database record.
     */
    @Override
    public String toString() {
        // Build a display string representing an Enrolment object.
        String retValue = "Room ID: " + roomId
                + ", Child ID: " + childId;

        return retValue;
    }
}