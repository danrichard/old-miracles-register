package ca.miracles.register.models;

/**
 * @author  Dan Richard
 * @since   2015-02-15
 */
public class Room {
    private long id;
    private String roomName;
    private int numOfStaff;
    private int fromMonths;
    private int toMonths;
    private String logoPath;
    private int totalEnrolled;
    private float ratio;
    private String ratioStatus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getNumOfStaff() {
        return numOfStaff;
    }

    public void setNumOfStaff(int numOfStaff) {
        this.numOfStaff = numOfStaff;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public int getFromMonths() {
        return fromMonths;
    }

    public void setFromMonths(int fromMonths) {
        this.fromMonths = fromMonths;
    }

    public int getToMonths() {
        return toMonths;
    }

    public void setToMonths(int toMonths) {
        this.toMonths = toMonths;
    }

    public int getTotalEnrolled() {
        return totalEnrolled;
    }

    public void setTotalEnrolled(int totalEnrolled) {
        this.totalEnrolled = totalEnrolled;
    }

    public float getRatio() {
        return ratio;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    public String getRatioStatus() {
        return ratioStatus;
    }

    public void setRatioStatus(String ratioStatus) {
        this.ratioStatus = ratioStatus;
    }

    /**
     * Text display of a Room record in the database.
     * @return  String representing a database record.
     */
    @Override
    public String toString() {
        // Build a display string representing a Room object.
        String retValue = "Room: " + roomName
                + ", Current Staff: " + numOfStaff
                + ", From age (mos.): " + fromMonths + " to: " + toMonths
                + ", Logo Path: " + logoPath;

        return retValue;
    }
}