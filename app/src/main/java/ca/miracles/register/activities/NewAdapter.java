package ca.miracles.register.activities;

/**
 * Created by Dan on 22/02/2015.
 */
import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;

import ca.miracles.register.R;
import ca.miracles.register.models.Child;

@SuppressWarnings("unchecked")
public class NewAdapter extends BaseExpandableListAdapter {

    public ArrayList<String> mGroupItem;
    public ArrayList<Child> mTempChild;
    public ArrayList<Object> mChildren = new ArrayList<Object>();
    public LayoutInflater mInflater;
    public Activity mActivity;

    public NewAdapter(ArrayList<String> groupList, ArrayList<Object> childItem) {
        mGroupItem = groupList;
        mChildren = childItem;
    }

    public void setInflater(LayoutInflater inflater, Activity activity) {
        mInflater = inflater;
        mActivity = activity;
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }


    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        mTempChild = (ArrayList<Child>) mChildren.get(groupPosition);
        TextView text = null;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.room_list_item, null);
        }

        text = (TextView) convertView.findViewById(R.id.lblListItem);
        text.setText(mTempChild.get(childPosition).toString());

        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, mTempChild.get(childPosition).toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return ((ArrayList<String>) mChildren.get(groupPosition)).size();
    }


    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }


    @Override
    public int getGroupCount() {
        return mGroupItem.size();
    }


    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }


    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }


    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.room_list_group, null);
        }

        ((CheckedTextView) convertView).setText(mGroupItem.get(groupPosition));
        ((CheckedTextView) convertView).setChecked(isExpanded);

        return convertView;
    }


    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}