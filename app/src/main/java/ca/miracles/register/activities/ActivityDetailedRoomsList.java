package ca.miracles.register.activities;

import android.app.ExpandableListActivity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ca.miracles.register.data.DataSourceChild;
import ca.miracles.register.data.DataSourceEnrolment;
import ca.miracles.register.data.DataSourceRoom;
import ca.miracles.register.models.Child;
import ca.miracles.register.models.Enrolment;
import ca.miracles.register.models.Room;


/**
 * @author  Dan Richard
 * @since   2015-02-21
 */
public class ActivityDetailedRoomsList extends ExpandableListActivity implements ExpandableListView.OnChildClickListener {
    ArrayList<String> groupItem = new ArrayList<String>();
    ArrayList<Object> childItem = new ArrayList<Object>();

    DataSourceRoom dsRooms = null;
    DataSourceEnrolment dsEnrolments = null;
    DataSourceChild dsChild = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExpandableListView expandableList = getExpandableListView();
        expandableList.setDividerHeight(2);
        expandableList.setGroupIndicator(null);
        expandableList.setClickable(true);

        setGroupData();

        NewAdapter mNewAdapter = new NewAdapter(groupItem, childItem);
        mNewAdapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE), this);
        getExpandableListView().setAdapter(mNewAdapter);
        expandableList.setOnChildClickListener(this);
    }

    public void setGroupData() {
        dsRooms = new DataSourceRoom(this);

        try {
            dsRooms.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        // Retrieve all Room records.
        List<Room> rooms = dsRooms.getAllRooms();

        for (Room room : rooms) {
            groupItem.add(room.getRoomName());

            // Add child records to the Hashmap.
            // Can assume a safe cast to int since the ID won't surpass the Integer capacity.
            setChildGroupData((int)room.getId());
        }
    }

    public void setChildGroupData(int roomId) {
        dsEnrolments = new DataSourceEnrolment(this);
        dsChild = new DataSourceChild(this);

        try {
            dsEnrolments.open();
            dsChild.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        // Retrieve all Enrolment records.
        List<Enrolment> enrolments = dsEnrolments.getEnrolmentsByRoom(roomId);
        ArrayList<Child> child = new ArrayList<Child>();

        for (Enrolment enrolment : enrolments) {
            // Retrieve the Child record details.
            Child childRecord = dsChild.getChild(enrolment.getChildId());
            child.add(childRecord);
        }

        childItem.add(child);
    }


    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Toast.makeText(ActivityDetailedRoomsList.this, "Clicked on Child", Toast.LENGTH_SHORT).show();
        return true;
    }
}