package ca.miracles.register.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.sql.SQLException;
import java.util.List;

import ca.miracles.register.R;
import ca.miracles.register.data.DataSourceChild;
import ca.miracles.register.data.DataSourceEnrolment;
import ca.miracles.register.data.DataSourceRoom;
import ca.miracles.register.models.Enrolment;
import ca.miracles.register.utils.SpinnerObject;

/**
 * @author  Dan Richard
 * @since   2015-02-15
 */
public class ActivityEnrolChild extends ListActivity implements View.OnClickListener {
    private DataSourceEnrolment dsEnrolment;

    // UI References
    Spinner spinRooms, spinChildren;

    // Variables to store form field values
    int mRoomId, mChildId;


    /**
     * On activity creation, set the layout view to render and prepare datasource(s) and initial datasets.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrol_child);

        dsEnrolment = new DataSourceEnrolment(this);
        try {
            dsEnrolment.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        // Retrieve all records and bind them to a listview.
        List<Enrolment> values = dsEnrolment.getAllEnrolments();
        ArrayAdapter<Enrolment> adapter = new ArrayAdapter<Enrolment>(this, android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);

        spinRooms = (Spinner)findViewById(R.id.room_spinner);


        spinChildren = (Spinner)findViewById(R.id.child_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> spinChildAdapter = ArrayAdapter.createFromResource(this,
                R.array.current_staff_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        spinChildAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinChildren.setAdapter(spinChildAdapter);

        // Loading spinner data from database
        loadSpinnerData();
    }


    /**
     * Button processor.
     * Performs onClick handling for Adding and Deleting Child rows.
     * @param view
    */
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<Enrolment> adapter = (ArrayAdapter<Enrolment>) getListAdapter();
        Enrolment enrolment = null;
        switch (view.getId()) {
            // Process button click for adding a record
            case R.id.add:
                // Get values from form fields.
                mRoomId = ((SpinnerObject)spinRooms.getSelectedItem()).getId();
                mChildId = ((SpinnerObject)spinChildren.getSelectedItem()).getId();

                // Save the new record to the database.
                enrolment = dsEnrolment.createEnrolment(mRoomId, mChildId);
                adapter.add(enrolment);

                break;
            // Process button click for deleting a record
            case R.id.delete:
                // Delete first item in the list.
                // TODO: This needs to change to delete the record selected
                if (getListAdapter().getCount() > 0) {
                    enrolment = (Enrolment) getListAdapter().getItem(0);
                    dsEnrolment.deleteEnrolment(enrolment);
                    adapter.remove(enrolment);
                }
                break;
        }
        // Refresh the ListView on-screen.
        adapter.notifyDataSetChanged();
    }


    /**
     * Inflate the menu into the current view.
     * @param menu  The menu to inflate.
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Handle menu selections.
     * @param item  Selected menu item.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_add_child:
                Intent intentAddChild = new Intent(this, ActivityAddChild.class);
                this.startActivity(intentAddChild);
                break;
            case R.id.action_add_room:
                Intent intentAddRoom = new Intent(this, ActivityEnrolChild.class);
                this.startActivity(intentAddRoom);
                break;
            case R.id.action_enrol_child:
                // Do nothing. We're already on this activity.
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    /**
     * Function to load the spinner data from SQLite database
     */
    private void loadSpinnerData() {
        DataSourceRoom dsRooms;
        DataSourceChild dsChildren;

        dsRooms = new DataSourceRoom(this);
        try {
            dsRooms.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        // Spinner Drop down elements
        List<SpinnerObject> rooms = dsRooms.getRoomsForSpinner();
        // Creating adapter for spinner
        ArrayAdapter<SpinnerObject> daRooms = new ArrayAdapter<SpinnerObject>(this,
                android.R.layout.simple_spinner_item, rooms);
        // Drop down layout style - list view with radio button
        daRooms.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinRooms.setAdapter(daRooms);

        // Repeat logic above for list of Children
        dsChildren = new DataSourceChild(this);
        try {
            dsChildren.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        List<SpinnerObject> children = dsChildren.getChildrenForSpinner();
        ArrayAdapter<SpinnerObject> daChildren = new ArrayAdapter<SpinnerObject>(this,
                android.R.layout.simple_spinner_item, children);
        daChildren.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinChildren.setAdapter(daChildren);
    }


    /**
     * When the app returns from the background, re-open the data connection.
     */
    @Override
    protected void onResume() {
        try {
            dsEnrolment.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }
        super.onResume();
    }


    /**
     * When the app moves to the background, close the database connection.
     */
    @Override
    protected void onPause() {
        dsEnrolment.close();
        super.onPause();
    }
}
