package ca.miracles.register.activities;

import android.app.DatePickerDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import ca.miracles.register.data.DataSourceEnrolment;
import ca.miracles.register.data.DataSourceRoom;
import ca.miracles.register.models.Child;
import ca.miracles.register.data.DataSourceChild;
import ca.miracles.register.R;
import ca.miracles.register.models.Enrolment;
import ca.miracles.register.utils.SpinnerObject;
import ca.miracles.register.utils.Converters;

/**
 * @author  Dan Richard
 * @since   2015-02-09
 */
public class ActivityAddChild extends ListActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private DataSourceChild datasource;

    // UI References
    ImageButton btnDobCalendar, btnWaitListCalendar;
    EditText txtFName, txtLName, txtPhone, txtDobDate, txtWaitingListDate;
    CheckBox chkWaitList;
    Spinner spinRooms;

    // Variables to store form field values
    String mFName, mLName, mPhone;
    int mWaitList, mRoomId, mChildId;

    // Variable for storing current date and time
    private int mYear, mMonth, mDay;


    /**
     * On activity creation, set the layout view to render and prepare datasource(s) and initial datasets.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);

        datasource = new DataSourceChild(this);
        try {
            datasource.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        // Retrieve all records and bind them to a listview.
        List<Child> values = datasource.getAllChildren();
        ArrayAdapter<Child> adapter = new ArrayAdapter<Child>(this, android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);

        txtFName = (EditText)findViewById(R.id.fname_text);
        txtLName = (EditText)findViewById(R.id.lname_text);
        txtPhone = (EditText)findViewById(R.id.phone_text);
        chkWaitList = (CheckBox)findViewById(R.id.waitinglist_chk);
        txtDobDate = (EditText)findViewById(R.id.dob_text);
        txtWaitingListDate = (EditText)findViewById(R.id.waitinglistdate_text);

        btnDobCalendar = (ImageButton)findViewById(R.id.btnDobCalendar);
        btnWaitListCalendar = (ImageButton)findViewById(R.id.btnWaitListCalendar);

        btnDobCalendar.setOnClickListener(this);
        btnWaitListCalendar.setOnClickListener(this);
        addListenerOnChkWaitList();

        spinRooms = (Spinner)findViewById(R.id.room_spinner);
        spinRooms.setOnItemSelectedListener(this);

        // Loading spinner data from database
        loadSpinnerData();
    }


    /**
     * Listener method for the Waiting List checkbox.
     */
    public void addListenerOnChkWaitList() {
        chkWaitList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mWaitList = 1;
                } else {
                    mWaitList = 0;
                }
            }
        });
    }


    /**
     * Button processor.
     * Performs onClick handling for Adding and Deleting Child rows.
     * @param view
    */
    public void onClick(View view) {

        @SuppressWarnings("unchecked")
        ArrayAdapter<Child> adapter = (ArrayAdapter<Child>) getListAdapter();
        Child child = null;
        switch (view.getId()) {
            // Process buttons clicks for Calendar popups
            case R.id.btnDobCalendar:
            case R.id.btnWaitListCalendar:
                // Process button clicks for Date Pickers
                if (view == btnDobCalendar || view == btnWaitListCalendar) {
                    // Process to get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    if(view == btnDobCalendar) {
                        // Launch Date Picker Dialog
                        DatePickerDialog dpd = new DatePickerDialog(this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        // Display Selected date in textbox
                                        // "MMM dd yyyy"
                                        txtDobDate.setText(Converters.dateDisplay(year, (monthOfYear + 1), dayOfMonth));
                                    }
                                }, mYear, mMonth, mDay);
                        dpd.show();
                    } else if(view == btnWaitListCalendar) {
                        // Launch Date Picker Dialog
                        DatePickerDialog dpd = new DatePickerDialog(this,
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        // Display Selected date in textbox
                                        txtWaitingListDate.setText(Converters.dateDisplay(year, (monthOfYear + 1), dayOfMonth));
                                    }
                                }, mYear, mMonth, mDay);
                        dpd.show();
                    }
                }
                break;
            // Process button click for adding a record
            case R.id.add:
                // Get values from form fields.
                mFName = txtFName.getText().toString();
                mLName = txtLName.getText().toString();
                mPhone = txtPhone.getText().toString();

                // Save the new record to the database.
                child = datasource.createChild(mFName,
                        mLName,
                        mPhone,
                        Converters.stringToCalendar(txtDobDate.getText().toString()),
                        mWaitList,
                        Converters.stringToCalendar(txtWaitingListDate.getText().toString()));
                adapter.add(child);

                // Save an Enrolment record if a room was selected.
                // If RoomID exists, then a room was selected from the Spinner.
                if(mRoomId > 0) {
                    // Get child record ID of the record that was just added.
                    // We can assume safe cast from Long to Int since # of records would never hit the max of Int capacity.
                    mChildId = (int)child.getId();
                    enrolChild();
                }
                break;
            // Process button click for deleting a record
            case R.id.delete:
                // Delete first item in the list.
                // TODO: This needs to change to delete the record selected
                if (getListAdapter().getCount() > 0) {
                    child = (Child) getListAdapter().getItem(0);
                    datasource.deleteChild(child);
                    adapter.remove(child);
                }
                break;
        }
        // Refresh the ListView on-screen.
        adapter.notifyDataSetChanged();
    }


    /**
     * Inflate the menu into the current view.
     * @param menu  The menu to inflate.
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Handle menu selections.
     * @param item  Selected menu item.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_add_child:
                // Do nothing. We're already on this activity.
                break;
            case R.id.action_add_room:
                Intent intentAddRoom = new Intent(this, ActivityAddRoom.class);
                this.startActivity(intentAddRoom);
                break;
            case R.id.action_enrol_child:
                Intent intentEnrolChild = new Intent(this, ActivityEnrolChild.class);
                this.startActivity(intentEnrolChild);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    /**
     * When the app returns from the background, re-open the data connection.
     */
    @Override
    protected void onResume() {
        try {
            datasource.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }
        super.onResume();
    }


    /**
     * When the app moves to the background, close the database connection.
     */
    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }


    /**
     * Create an Enrolment record for the current Child being added.
     */
    private void enrolChild() {
        DataSourceEnrolment dsEnrolment;
        Enrolment enrolment = null;

        dsEnrolment = new DataSourceEnrolment(this);
        try {
            dsEnrolment.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        enrolment = dsEnrolment.createEnrolment(mRoomId, mChildId);
    }


    //TODO: Add an unenrolChild() method for deletes.


    /**
     * Function to load the spinner data from SQLite database
     */
    private void loadSpinnerData() {
        DataSourceRoom dsRooms;

        dsRooms = new DataSourceRoom(this);
        try {
            dsRooms.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        // Spinner Drop down elements
        List<SpinnerObject> rooms = dsRooms.getRoomsForSpinner();
        // Creating adapter for spinner
        ArrayAdapter<SpinnerObject> daRooms = new ArrayAdapter<SpinnerObject>(this,
                android.R.layout.simple_spinner_item, rooms);
        // Drop down layout style - list view with radio button
        daRooms.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinRooms.setAdapter(daRooms);
    }


    /**
     * Retrieve the ID value of the selected room from the Spinner.
     * @param parent
     * @param view
     * @param pos
     * @param id
     */
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        mRoomId = ((SpinnerObject)spinRooms.getSelectedItem()).getId();
    }


    /**
     * Abstract method is required.
     * @param parent
     */
    public void onNothingSelected(AdapterView parent) {
        // Do nothing.
    }
}
