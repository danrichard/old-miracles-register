package ca.miracles.register.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import java.sql.SQLException;
import java.util.List;

import ca.miracles.register.R;
import ca.miracles.register.data.DataSourceRoom;
import ca.miracles.register.models.Room;

/**
 * @author  Dan Richard
 * @since   2015-02-15
 */
public class ActivityAddRoom extends ListActivity implements View.OnClickListener {
    private DataSourceRoom dsRoom;

    // UI References
    EditText txtRoomName, txtLogoPath;
    Spinner spinNumOfStaff;
    NumberPicker pickAgeFrom, pickAgeTo;

    // Variables to store form field values
    String mRoomName, mLogoPath;
    int mNumOfStaff, mAgeFrom, mAgeTo;


    /**
     * On activity creation, set the layout view to render and prepare datasource(s) and initial datasets.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_room);

        dsRoom = new DataSourceRoom(this);
        try {
            dsRoom.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }

        // Retrieve all records and bind them to a listview.
        List<Room> values = dsRoom.getAllRooms();
        ArrayAdapter<Room> adapter = new ArrayAdapter<Room>(this, android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);

        txtRoomName = (EditText)findViewById(R.id.room_name_text);

        spinNumOfStaff = (Spinner)findViewById(R.id.room_staff_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> spinAdapter = ArrayAdapter.createFromResource(this,
                R.array.current_staff_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinNumOfStaff.setAdapter(spinAdapter);

        pickAgeFrom = (NumberPicker)findViewById(R.id.room_age_from_picker);
        pickAgeFrom.setMaxValue(60);
        pickAgeFrom.setMinValue(1);
        pickAgeFrom.setWrapSelectorWheel(true);

        pickAgeTo = (NumberPicker)findViewById(R.id.room_age_to_picker);
        pickAgeTo.setMaxValue(60);
        pickAgeTo.setMinValue(1);
        pickAgeTo.setWrapSelectorWheel(true);

        txtLogoPath = (EditText)findViewById(R.id.room_logo_text);
    }


    /**
     * Button processor.
     * Performs adds & deletes in the listview bound to the dataset.
     * @param view
    */
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        ArrayAdapter<Room> adapter = (ArrayAdapter<Room>) getListAdapter();
        Room room = null;
        switch (view.getId()) {
            // Process button click for adding a record
            case R.id.add:
                // Get values from form fields.
                mRoomName = txtRoomName.getText().toString();
                mNumOfStaff = Integer.parseInt(spinNumOfStaff.getSelectedItem().toString());
                mAgeFrom = pickAgeFrom.getValue();
                mAgeTo = pickAgeTo.getValue();
                mLogoPath = txtLogoPath.getText().toString();

                // Save the new record to the database.
                room = dsRoom.createRoom(mRoomName,
                        mNumOfStaff,
                        mAgeFrom,
                        mAgeTo,
                        mLogoPath);
                adapter.add(room);

                break;
            // Process button click for deleting a record
            case R.id.delete:
                // Delete first item in the list.
                // TODO: This needs to change to delete the record selected
                if (getListAdapter().getCount() > 0) {
                    room = (Room) getListAdapter().getItem(0);
                    dsRoom.deleteRoom(room);
                    adapter.remove(room);
                }
                break;
        }
        // Refresh the ListView on-screen.
        adapter.notifyDataSetChanged();
    }


    /**
     * Inflate the menu into the current view.
     * @param menu  The menu to inflate.
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Handle menu selections.
     * @param item  Selected menu item.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_add_child:
                Intent intentAddChild = new Intent(this, ActivityAddChild.class);
                this.startActivity(intentAddChild);
                break;
            case R.id.action_add_room:
                // Do nothing. We're already on this activity.
                break;
            case R.id.action_enrol_child:
                Intent intentEnrolChild = new Intent(this, ActivityEnrolChild.class);
                this.startActivity(intentEnrolChild);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    /**
     * When the app returns from the background, re-open the data connection.
     */
    @Override
    protected void onResume() {
        try {
            dsRoom.open();
        } catch (SQLException e) {
            Log.e("Error opening database", e.toString());
        }
        super.onResume();
    }


    /**
     * When the app moves to the background, close the database connection.
     */
    @Override
    protected void onPause() {
        dsRoom.close();
        super.onPause();
    }
}
